package com.example.krist.lab3;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.VibrationEffect;
import android.os.Vibrator;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

//Based on https://gist.github.com/Jawnnypoo/fcceea44be628c2d5ae1

public class MainActivity extends AppCompatActivity implements SensorEventListener {

    private SensorManager sensorManager;
    private Sensor accelerometer;

    private AnimatedView animatedView;
    private MediaPlayer mediaPlayer;
    private Vibrator vibrator;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mediaPlayer = MediaPlayer.create(MainActivity.this, R.raw.pingsound);
        vibrator = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);


        animatedView = new AnimatedView(this);
        setContentView(animatedView);
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sensorManager.unregisterListener(this);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        {
            if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                animatedView.onSensorEvent(sensorEvent);
            }
        }
    }


    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {

    }


    public class AnimatedView extends View {

        int width;
        int height;
        int padding;
        int ballX, ballY;
        int ballRad;

        boolean start;
        boolean vib;

        Paint mInnerRectanglePaint;
        Paint mBallPaint;


        public AnimatedView(Context context) {
            super(context);
            setContentView(this);
            padding = 10;
            ballRad = (int)(25);

            start = true;

            mInnerRectanglePaint = new Paint();
            mInnerRectanglePaint.setStyle(Paint.Style.FILL);
            mInnerRectanglePaint.setColor(Color.BLACK);

            mBallPaint = new Paint();
            mBallPaint.setStyle(Paint.Style.FILL);
            mBallPaint.setColor(Color.WHITE);
        }

        @Override
        protected void onSizeChanged(int w, int h, int oldw, int oldh) {
            super.onSizeChanged(w, h, oldw, oldh);

            width = w;
            height = h;
        }

        //Handles updating positiong, pings, and vibrations when tilting
        public void onSensorEvent(SensorEvent sensorEvent) {
            ballY += sensorEvent.values[0];
            ballX += sensorEvent.values[1];

            if (ballX <= 0 + ballRad) {
                ballX = 0 + ballRad;
                mediaPlayer.start();
                vib = true;
            }
            if (ballX >= width - padding - ballRad) {
                ballX = width - padding - ballRad;
                mediaPlayer.start();
                vib = true;
            }
            if (ballY <= 0 + ballRad) {
                ballY = 0 + ballRad;
                mediaPlayer.start();
                vib = true;
            }
            if (ballY >= height - padding - ballRad) {
                ballY = height - padding - ballRad;
                mediaPlayer.start();
                vib = true;
            }

            if (vib) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
                    vibrator.vibrate(VibrationEffect.createOneShot(200, VibrationEffect.DEFAULT_AMPLITUDE));
                else
                    vibrator.vibrate(200);

                vib = false;
            }

        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawRect( padding, padding, width - padding, height - padding, mInnerRectanglePaint);

            // To set starting point to center. Still pings/vibrates at start ¯\_(ツ)_/¯
            if (start) {
                canvas.drawCircle(width / 2, height / 2, ballRad, mBallPaint);
                ballX = width / 2;
                ballY = height / 2;
                start = false;
            } else
                canvas.drawCircle(ballX, ballY, ballRad, mBallPaint);

            invalidate();
        }
    }
}
