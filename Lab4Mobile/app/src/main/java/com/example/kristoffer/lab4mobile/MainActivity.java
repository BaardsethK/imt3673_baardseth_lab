package com.example.kristoffer.lab4mobile;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity {

    public ChatActivity mChatActivity;
    public UserActivity mUserActivity;
    public UserMessagesActivity mUserMessagesActivity;

    //private FirebaseFirestore mFirebaseFirestore;
    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;

    private String mUsername;

    String mUserName;

    EditText userNameText;
    Button acceptButton;

    SharedPreferences mSharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mChatActivity = new ChatActivity();


        acceptButton = findViewById(R.id.AcceptButton);

        mFirebaseAuth = FirebaseAuth.getInstance();
        mSharedPreferences = this.getPreferences(Context.MODE_PRIVATE);
        mUserName = mSharedPreferences.getString("username", "");

        userNameText.setText(mUserName, TextView.BufferType.EDITABLE);

        acceptButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LoginUser();
            }
        });
    }

    private void LoginUser() {

    }

}
