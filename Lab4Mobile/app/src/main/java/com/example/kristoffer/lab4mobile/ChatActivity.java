package com.example.kristoffer.lab4mobile;

import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class ChatActivity extends AppCompatActivity {

    private DatabaseReference mDatabase;

    LinearLayout chatTab;
    LinearLayout userListTab;
    Button sendButton;
    ListView chatList;
    ListView userList;

    TextView messageText;
    String incomingMessage;
    String outgoingMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        mDatabase = FirebaseDatabase.getInstance().getReference();

        chatTab = findViewById(R.id.ChatFeedTab);
        chatList = findViewById(R.id.ChatList);

        userListTab = findViewById(R.id.UserListTab);
        userList = findViewById(R.id.UserList);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SendMessage();
            }
        });

    }

    private void SendMessage() {
    }

    public void UpdateChat(){
    }
}
