package com.example.krist.mobilelab4_new;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentChange;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QuerySnapshot;

import java.util.HashMap;

public class LoginActivity extends AppCompatActivity {

    private static final String TAG = "LOGINACTIVITY";

    private DatabaseCommunication mDatabaseComm;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore mFirestore;
    private FirebaseUser mFirebaseUser;

    private EditText mUsernameEditText;
    private Button mAccessButton;

    private SharedPreferences mSharedPreferences;

    private String mUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mDatabaseComm = new DatabaseCommunication();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        mSharedPreferences = getSharedPreferences("SharedPrefs", 0);
        mUsername = mSharedPreferences.getString("username", "UNASSIGNED");

        mUsernameEditText = findViewById(R.id.usernameEditText);
        mAccessButton = findViewById(R.id.accessButton);

        mAccessButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreateNewUser();
            }
        });

    }

    @Override
    protected void onStart() {
        super.onStart();
        //CheckCredentials();
        InitializeDatabaseStream();
        if (mUsername != "UNASSIGNED") {
           SignInAnonymously();
        }
    }

    public void SignInAnonymously() {
        mFirebaseAuth.signInAnonymously()
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            //Sign-in successful, pass user to Main Screen/chat
                            Log.d(TAG, "signInAnonymously:success");
                            mFirebaseUser = mFirebaseAuth.getCurrentUser();
                            SendToChatPage();
                        } else {
                            Log.w(TAG, "signInAnonymously:failure");
                        }

                    }
                });
    }

    private void CreateNewUser() {
        if (mUsernameEditText.getText().length() > 0) {
            mUsername = mUsernameEditText.getText().toString();
            SharedPreferences.Editor mEditor = mSharedPreferences.edit();
            mEditor.putString("username", mUsername);
            mEditor.apply();

            HashMap<String, String> mUser = new HashMap<>();
            mUser.put("username", mUsername);

            mDatabaseComm.createNewUser(mUser, mFirestore);
            SendToChatPage();
        } else {
            //Show error: Username too short
        }



    }

    private void SendToChatPage() {
        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
        startActivity(intent);
    }

    //TODO This is for constant dataflow - update messages/users
    public void InitializeDatabaseStream() {
        mFirestore.collection("users").addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot snapshots,
                                @Nullable FirebaseFirestoreException e) {
                if (e != null) {
                    Log.w(TAG, "listen:error", e);
                    return;
                }

                for (DocumentChange dc : snapshots.getDocumentChanges()) {
                    if (dc.getType() == DocumentChange.Type.ADDED){

                    }
                        //CreateNewUser((HashMap) dc.getDocument().getData());
                }
            }
        });
    }
}
