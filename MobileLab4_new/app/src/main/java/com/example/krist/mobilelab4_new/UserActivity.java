package com.example.krist.mobilelab4_new;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class UserActivity extends Fragment {

    private ListView mUserListView;

    private ArrayAdapter<String> mArrayAdapter;
    private List<String> mUsers;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container, Bundle savedInstance) {
        View view = layoutInflater.inflate(R.layout.userlist_fragment, container, false);

        mUserListView = view.findViewById(R.id.userListView);
        //mUserListView.setOnItemClickListener();

        mUsers = new ArrayList<>();
        mArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mUsers);
        mUserListView.setAdapter(mArrayAdapter);

        return view;
    }

    public void AddUser(String username) {
        if (mUsers != null) {
            mUsers.add(username);
            Collections.sort(mUsers, String.CASE_INSENSITIVE_ORDER);
            mArrayAdapter.notifyDataSetChanged();
        }
    }

    public void Clear() {
        if (mUsers != null) {
            mUsers.clear();
        }
    }
}
