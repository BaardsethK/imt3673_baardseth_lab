package com.example.krist.mobilelab4_new;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class UserMessagesActivity extends Fragment {
    private ListView mUserMessagesListView;
    private ArrayAdapter<String> mArrayAdapter;
    private List<String> mMessages;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View view = layoutInflater.inflate(R.layout.usermessages_fragment, container, false);

        mUserMessagesListView = view.findViewById(R.id.userMessagesListView);

        mMessages = new ArrayList<>();
        mArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mMessages);
        mUserMessagesListView.setAdapter(mArrayAdapter);

        return view;
    }

    public void addMessage(String message) {
        mMessages.add(message);
        Collections.sort(mMessages);
        mArrayAdapter.notifyDataSetChanged();
    }
}
