package com.example.krist.mobilelab4_new;

import com.google.firebase.Timestamp;

public class Message {
    private String mUsername;
    private String mMessage;

    private Timestamp mTimestamp;

    Message(String username, String message, Timestamp timestamp) {
        this.mUsername = username;
        this.mMessage = message;
        this.mTimestamp = timestamp;
    }
}
