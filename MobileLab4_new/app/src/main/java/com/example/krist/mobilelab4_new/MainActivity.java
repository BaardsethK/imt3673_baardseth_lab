package com.example.krist.mobilelab4_new;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;

import com.google.firebase.Timestamp;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.firestore.FirebaseFirestore;

public class MainActivity extends AppCompatActivity {

    private FragmentAdapter mFragmentAdapter;
    private ChatActivity mChatActivity;
    private UserActivity mUserActivity;
    private UserMessagesActivity mUserMessagesActivity;

    private FirebaseAuth mFirebaseAuth;
    private FirebaseFirestore mFirestore;
    private DatabaseCommunication mDatabaseComm;

    private ViewPager mViewPager;
    private TabLayout mTabLayout;

    private String mUsername;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mDatabaseComm = new DatabaseCommunication();
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirestore = FirebaseFirestore.getInstance();

        SharedPreferences mSharedPreferences = getSharedPreferences("SharedPrefs", 0);
        mUsername = mSharedPreferences.getString("username", "");

        mChatActivity = new ChatActivity();
        mUserActivity = new UserActivity();

        mFragmentAdapter = new FragmentAdapter(getSupportFragmentManager());
        mViewPager = findViewById(R.id.viewPager);
        InitializeViewPager(mViewPager);

        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (mUserMessagesActivity != null && position < 2) {
                    mFragmentAdapter.removeUserMessagesFragment();
                    mFragmentAdapter.notifyDataSetChanged();
                    mViewPager.setCurrentItem(1);
                    mUserMessagesActivity = null;
                    clearFragments();
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });


        mTabLayout = findViewById(R.id.tabLayout);
        mTabLayout.setupWithViewPager(mViewPager);


    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    private void clearFragments() {
        mChatActivity.Clear();
        mUserActivity.Clear();
    }

    private void InitializeFragments() {

    }

    private void InitializeViewPager(ViewPager mViewPager){
        mFragmentAdapter.addFragment(mChatActivity, "Chat");
        mFragmentAdapter.addFragment(mUserActivity, "Users");
        mViewPager.setAdapter(mFragmentAdapter);
    }

    public void sendMessage(String s) {
        Message message = new Message(mUsername, s, Timestamp.now());
        mDatabaseComm.sendMessage(message, mFirestore);
    }
}
