package com.example.krist.mobilelab4_new;

import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ChatActivity extends Fragment {

    private ListView mMessageListView;
    private EditText mMessageText;
    private Button mSendButton;

    private ArrayAdapter<String> mArrayAdapter;
    private List<String> mMessages;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater layoutInflater, @Nullable ViewGroup container, Bundle savedInstance) {
        View view = layoutInflater.inflate(R.layout.chat_fragment, container, false);

        mMessageListView = view.findViewById(R.id.chatListView);
        mMessageText = view.findViewById(R.id.messageEditText);
        mSendButton = view.findViewById(R.id.sendMessageButton);

        mSendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MainActivity mMainActivity = (MainActivity) getActivity();
                if (mMessageText.getText().length() > 0) {
                    mMainActivity.sendMessage(mMessageText.getText().toString());
                    mMessageText.setText("");
                }
            }
        });

        mMessages = new ArrayList<>();
        mArrayAdapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_1, mMessages);
        mMessageListView.setAdapter(mArrayAdapter);

        return view;
    }

    public List<String> GetMessages() {return mMessages;}

    public void AddMessage(String message) {
        if (mMessages != null) {
            mMessages.add(message);
            Collections.sort(mMessages);
            mArrayAdapter.notifyDataSetChanged();
            mMessageListView.setSelection(mArrayAdapter.getCount() - 1);
        }
    }

    public void Clear() {
        if (mMessages != null) {
            mMessages.clear();
        }
    }
}
