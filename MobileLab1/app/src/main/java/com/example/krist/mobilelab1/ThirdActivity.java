package com.example.krist.mobilelab1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class ThirdActivity extends AppCompatActivity {

    private EditText editText;
    private Button enterBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);

        addListenerOnButton();
        addListenerOnTextField();
    }

    public void addListenerOnButton() {
        enterBtn = findViewById(R.id.button3);
        enterBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendInformation(view);
            }
        });
    }

    public void addListenerOnTextField() {

    }

    private void sendInformation(View view) {
        EditText editText = findViewById(R.id.editText2);
        String info = editText.getText().toString();
        Intent returnInfoIntent = new Intent();
        if (!info.isEmpty()) {
            returnInfoIntent.putExtra("Info", info);
            setResult(Activity.RESULT_OK, returnInfoIntent);
            finish();
        } else if (info.isEmpty()) {
            setResult(Activity.RESULT_CANCELED);
            finish();
        }
    }
}
