package com.example.krist.mobilelab1;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.text.BreakIterator;

public class SecondActivity extends AppCompatActivity {

    private Button nextBtn;
    private String a3Info;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

        Intent intent = getIntent();
        String message = intent.getStringExtra(EntryActivity.EXTRA_MESSAGE);

        TextView textView = findViewById(R.id.textView);
        TextView a3TextView = findViewById(R.id.textView2);
        textView.setText("Hello " + message);
        a3TextView.setText("From A3: " + a3Info);

        addListenerOnButton();
    }

    public void addListenerOnButton() {
        nextBtn = (Button) findViewById(R.id.button2);

        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getInformation(view);
            }
        });
    }

    public void updateText(String text) {
        TextView a3TextView = findViewById(R.id.textView2);
        a3TextView.setText("From A3: " + text);
    }

    private void getInformation(View view) {
        Intent getInfoIntent = new Intent(this, ThirdActivity.class);
        startActivityForResult(getInfoIntent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        String info;
        if (requestCode == 1) {
            if (resultCode == Activity.RESULT_OK) {
                info = data.getStringExtra("Info");
                updateText(info);
            }
            if (resultCode == Activity.RESULT_CANCELED) {
                //Do nothing
            }
        }
    }
}
