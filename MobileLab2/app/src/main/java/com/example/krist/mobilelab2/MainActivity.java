package com.example.krist.mobilelab2;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {

    private ListView listView;
    private Button fetchBtn;
    private Button settingsBtn;

    private String rssFeedTitle, rssFeedLink, rssFeedDesc;

    private ArrayList<RssFeedModel> feedModelList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fetchBtn = findViewById(R.id.fetchButton);
        settingsBtn = findViewById(R.id.settingsButton);

        fetchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new FetchFeed().execute();
            }
        });

        settingsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getNewSettings();
            }
        });

        startTimer();

    }

    private void startTimer() {
        SharedPreferences prefs = getSharedPreferences("UserPreferences", 0);

        Timer timer = new Timer();
        TimerTask fetchTask = new TimerTask() {
            @Override
            public void run() {
                new FetchFeed().execute();
            }
        };

        int[] temp = {10, 60, 1440};
        int period = temp[prefs.getInt("chosenRate", 0)];
        timer.schedule(fetchTask, 01, 1000*60*period);

    }

    private void getNewSettings(){
        Intent settingsIntent = new Intent(this, SettingsActivity.class);
        startActivityForResult(settingsIntent, 1);
    }

    public ArrayList<RssFeedModel> parseFeed(InputStream inputStream) throws XmlPullParserException, IOException {
        String title = null;
        String desc = null;
        String link = null;
        boolean item = false;
        ArrayList<RssFeedModel> items = new ArrayList<>();

        try {
            XmlPullParser xmlPullParser = Xml.newPullParser();
            xmlPullParser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
            xmlPullParser.setInput(inputStream, null);

            xmlPullParser.nextTag();
            while (xmlPullParser.next() != XmlPullParser.END_DOCUMENT) {
                int eventType = xmlPullParser.getEventType();

                String name = xmlPullParser.getName();

                if(name == null) {
                    continue;
                }

                if (eventType == XmlPullParser.END_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        item = false;
                    }
                    continue;
                }

                if (eventType == XmlPullParser.START_TAG) {
                    if (name.equalsIgnoreCase("item")) {
                        item = true;
                        continue;
                    }
                }

                String result = "";
                if (xmlPullParser.next() == XmlPullParser.TEXT) {
                    result = xmlPullParser.getText();
                    xmlPullParser.nextTag();
                }

                if (name.equalsIgnoreCase("title")) {
                    title = result;
                } else if (name.equalsIgnoreCase("link")) {
                    link = result;
                } else if (name.equalsIgnoreCase("description")) {
                    desc = result;
                }

                if (title != null && link != null && desc != null) {
                    if (item) {
                        RssFeedModel rssItem = new RssFeedModel(title, link, desc);
                        items.add(rssItem);
                    } else {
                        rssFeedTitle = title;
                        rssFeedLink = link;
                        rssFeedDesc = desc;
                    }

                    title = null;
                    link = null;
                    desc = null;
                    item = false;
                }
            }

            return items;
        } finally {
            inputStream.close();
        }

    }

    private class FetchFeed extends AsyncTask<Void, Void, Boolean> {

        private String urlLink;

        SharedPreferences prefs = getSharedPreferences("UserPreferences", 0);


        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            SharedPreferences prefs = getSharedPreferences("UserPreferences", 0);

            urlLink = prefs.getString("rssURL", "https://www.vg.no/rss/feed/forsiden");
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            if (urlLink.isEmpty())
                return false;

            try {
                if(!urlLink.startsWith("http://") && !urlLink.startsWith("https://"))
                    urlLink = "http://" + urlLink;

                URL url = new URL(urlLink);
                InputStream inputStream = url.openConnection().getInputStream();
                feedModelList = parseFeed(inputStream);
                return true;
            } catch (IOException e) {
                Log.e("MainActivity", "Error", e);
            } catch (XmlPullParserException e) {
                Log.e("MainActivity", "Error", e);
            }
            return false;
        }

        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);

            if(success) {
                updateFeed();
            }
        }

        public void updateFeed() {
            ListView feed = findViewById(R.id.RSSFeed);
            RSSAdapter rssAdapter = new RSSAdapter(MainActivity.this, feedModelList, prefs.getInt("chosenSize", 2));

            feed.setAdapter(rssAdapter);
            feed.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    // +1 for some weird reason
                    String url = ((RssFeedModel) adapterView.getItemAtPosition(i+1)).getLink();
                    Intent webActivity = new Intent(MainActivity.this, RSSItemActivity.class);
                    webActivity.putExtra("URLAddress", url);
                    startActivity(webActivity);
                }
            });
        }
    }
}
