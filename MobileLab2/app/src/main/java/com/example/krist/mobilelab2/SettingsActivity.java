package com.example.krist.mobilelab2;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;
import java.util.prefs.PreferenceChangeEvent;

public class SettingsActivity extends AppCompatActivity {

    private EditText rssURLText;
    private Spinner loadSizeSpinner;
    private Spinner refreshRateSpinner;
    private Button applyBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        SharedPreferences prefs = getSharedPreferences("UserPreferences", 0);

        rssURLText = findViewById(R.id.rssLinkTextEdit);
        loadSizeSpinner = findViewById(R.id.nrDisplaySpinner);
        refreshRateSpinner = findViewById(R.id.refreshRateSpinner);
        applyBtn = findViewById(R.id.applySettings);

        List<String> rateList = new ArrayList<>();
        rateList.add("10 min");
        rateList.add("1 hour");
        rateList.add("24 hours");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item, rateList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        refreshRateSpinner.setAdapter(adapter);
        refreshRateSpinner.setSelection(getPreference("chosenRate", prefs));

        List<String> sizeList = new ArrayList<>();
        sizeList.add("10");
        sizeList.add("20");
        sizeList.add("50");
        sizeList.add("100");

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, sizeList);
        adapter2.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        loadSizeSpinner.setAdapter(adapter2);
        loadSizeSpinner.setSelection(getPreference("chosenSize", prefs));

        rssURLText.setText(prefs.getString("rssURL", "Enter URL"));

        addListenerOnURLText();
        addListenerOnSizeSpinner();
        addListenerOnRateSpinner();
        addListenerOnRateButton();
    }

    private void addListenerOnRateButton() {
        applyBtn = findViewById(R.id.applySettings);
        applyBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendInformation(view);
            }
        });
    }

    private void sendInformation(View view) {
        finish();
    }

    private void addListenerOnRateSpinner() {
        refreshRateSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPref = getSharedPreferences("UserPreferences", 0);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("chosenRate", i);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Do nothing
            }
        });
    }

    private void addListenerOnSizeSpinner() {
        loadSizeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences sharedPref = getSharedPreferences("UserPreferences", 0);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt("chosenSize", i);
                editor.apply();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                //Do nothing
            }
        });
    }

    private void addListenerOnURLText() {
        rssURLText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //Do nothing
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                SharedPreferences sharedPref = getSharedPreferences("UserPreferences", 0);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString("rssURL", rssURLText.getText().toString());
                editor.apply();
            }

            @Override
            public void afterTextChanged(Editable editable) {
                //Do nothing
            }
        });
    }

    public int getPreference(String text, SharedPreferences pref) {
        return pref.getInt(text, 0);
    }
}
