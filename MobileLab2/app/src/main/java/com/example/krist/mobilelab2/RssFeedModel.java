package com.example.krist.mobilelab2;

/**
 * Created by krist on 16.03.2018.
 */

class RssFeedModel {
    public String title;
    public String link;
    public String description;

    public RssFeedModel(String title, String link, String desc) {
        this.title = title;
        this.link = link;
        this.description = desc;
    }

    public String getLink() {
        return link;
    }
}
