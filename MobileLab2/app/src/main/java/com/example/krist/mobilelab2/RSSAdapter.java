package com.example.krist.mobilelab2;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by krist on 16.03.2018.
 */

public class RSSAdapter extends BaseAdapter implements View.OnClickListener {

    ArrayList<RssFeedModel> infoList;
    private LayoutInflater inflater;
    private Context context;
    int maxSize;

    public RSSAdapter(Context context, ArrayList<RssFeedModel> infoList, int maxSize) {
        this.context = context;
        this.infoList = infoList;
        inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        int [] maxNumbers = {10, 20, 50, 100};
        this.maxSize = maxNumbers[maxSize];
    }

    @Override
    public void onClick(View view) {

    }

    @Override
    public int getCount() {
        if (maxSize > infoList.size()) {
            return infoList.size();
        } else {
            return maxSize;
        }
    }

    @Override
    public Object getItem(int i) {
        return infoList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        RssFeedModel temp = (RssFeedModel) getItem(i);
        View v = inflater.inflate(R.layout.rss_list_layout, viewGroup, false);

        TextView title = v.findViewById(R.id.rssItemTitle);
        TextView disc = v.findViewById(R.id.rssItemDesc);

        title.setText(temp.title);
        disc.setText(temp.description);
        notifyDataSetChanged();
        return v;

    }
}
